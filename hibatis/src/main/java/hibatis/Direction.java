package hibatis;

/**
 * Created by huangdachao on 2018/6/21 23:03.
 * 排序顺序
 */
public enum Direction {
    /**
     * 正序
     */
    desc,

    /**
     * 逆序
     */
    asc
}
