package hibatis;

import hibatis.annotation.GeneratedSql;
import hibatis.ext.HibatisInterceptor;
import hibatis.ext.InterceptorContext;
import hibatis.support.Reflection;
import hibatis.support.ResultInterceptor;
import hibatis.support.parse.HasManayStatementParser;
import hibatis.support.parse.EntityMeta;
import hibatis.support.parse.MapperStatementParser;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.mapper.MapperFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by huangdachao on 2018/6/17 01:06.
 */
@org.springframework.context.annotation.Configuration
@Import(ResultInterceptor.class)
public class HibatisAutoConfig implements ApplicationContextAware, ApplicationListener<ContextRefreshedEvent> {
    private static ApplicationContext applicationContext;
    private static List<HibatisInterceptor> interceptorsCache = new ArrayList<>();
    @Autowired(required = false) List<MapperFactoryBean> factoryBeans = new ArrayList<>();
    @Autowired(required = false) SqlSession sqlSession;
    @Autowired(required = false) List<HibatisInterceptor> interceptors = new ArrayList<>();

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (sqlSession != null) {
            sqlSession.getConfiguration().setMapUnderscoreToCamelCase(true);
        }

        factoryBeans.forEach(bean -> {
            Class<?> ec = Reflection.getEntityClass(bean.getMapperInterface());
            if (ec != null) {
                boolean entityMetaGenerated = EntityMeta.isGenerated(ec);
                Configuration conf = bean.getSqlSession().getConfiguration();
                MapperStatementParser parser = new MapperStatementParser(conf, bean.getMapperInterface());
                parser.parse();

                if (!entityMetaGenerated) {
                    EntityMeta em = EntityMeta.get(ec);
                    em.getSecondaryQueryFieldViewMap().keySet().forEach(f -> {
                        try {
                            HasManayStatementParser.generate(conf, em.getEntityClass().getDeclaredField(f));
                        } catch (NoSuchFieldException e) {
                            throw new RuntimeException(e);
                        }
                    });
                }
            }
        });

        interceptorsCache = interceptors;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        HibatisAutoConfig.applicationContext = applicationContext;
    }

    public static void intercept(InterceptorContext context) {
        AnnotationAttributes attr = AnnotatedElementUtils.getMergedAnnotationAttributes(context.getMethod(), GeneratedSql.class);
        if (!attr.getString("interceptor").isEmpty()) {
            try {
                Method method = context.getMapperClass().getMethod(attr.getString("interceptor"), InterceptorContext.class);
                if (Modifier.isStatic(method.getModifiers())) {
                    method.invoke(null, context);
                } else {
                    method.invoke(applicationContext.getBean(context.getMapperClass()), context);
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        } else if (!interceptorsCache.isEmpty()) {
            interceptorsCache.forEach(i -> i.intercept(context));
        }
    }
}
