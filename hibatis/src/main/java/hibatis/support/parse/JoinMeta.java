package hibatis.support.parse;

import hibatis.JoinType;
import hibatis.annotation.Filter;
import hibatis.annotation.Join;
import hibatis.annotation.JoinColumn;
import hibatis.support.StringUtils;

import java.util.*;

/**
 * Created by huangdachao on 2018/6/14 14:00.
 * 关联表的元数据信息
 */
public class JoinMeta {
    /**
     * 关联表表名
     */
    private Table table;
    private JoinType joinType;

    /**
     * 关联条件中引用的表名集合
     */
    private Set<Table> referTables = new HashSet<>();

    /**
     * 关联条件数组
     */
    private List<JoinConstraint> constraints = new ArrayList<>();

    private Filter[] filters;

    public JoinMeta(Join join, EntityMeta em) {
        this.table = new Table(join.table(), join.tblAlias());
        this.joinType = join.type();
        this.filters = join.filters();

        for (JoinColumn c : join.joinColumns()) {
            this.constraints.add(new JoinConstraint(em, c.column(), c.referTable(), c.referTblAlias(), c.referColumn()));
            this.referTables.add(StringUtils.isEmpty(c.referTable()) ? em.getTable() : new Table(c.referTable(), c.referTblAlias()));
        }

        for (Filter f : filters) {
            if (!"".equals(f.table()) && !join.table().equals(f.table())) {
                this.referTables.add(new Table(f.table(), f.tblAlias()));
            }
        }
    }

    public Table getTable() {
        return table;
    }

    public JoinType getJoinType() {
        return joinType;
    }

    public Set<Table> getReferTables() {
        return referTables;
    }

    public List<JoinConstraint> getConstraints() {
        return Collections.unmodifiableList(constraints);
    }

    public Filter[] getFilters() {
        return filters;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JoinMeta)) {
            return false;
        }
        JoinMeta joinMeta = (JoinMeta) o;
        return Objects.equals(table, joinMeta.table);
    }

    @Override
    public int hashCode() {
        return Objects.hash(table);
    }
}
