package hibatis.support;

import java.lang.reflect.Member;
import java.lang.reflect.Method;

/**
 * @author dchuang
 * Created: 2018/11/14 15:32
 * Description:
 */
public class PropertyMember implements Member {
    private String name;
    private Method method;

    public PropertyMember(String name, Method m) {
        this.name = name;
        this.method = m;
    }

    @Override
    public Class<?> getDeclaringClass() {
        return method.getDeclaringClass();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getModifiers() {
        return method.getModifiers();
    }

    @Override
    public boolean isSynthetic() {
        return true;
    }

    public Method getMethod() {
        return method;
    }
}
