package hibatis.annotation;

import java.lang.annotation.*;

/**
 * Created by huangdachao on 2018/6/21 23:00.
 * 用于标记排序字段或参数。该字段必须使用字符串格式，排序字段必须在实体类中声明。例如：
 * <code>groupId asc, createTime desc</code>
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.METHOD})
@Filter(ignore = true)
public @interface Sort {
}
